module samediprog.tv/tests-unitaires

go 1.14

require (
	github.com/Knetic/govaluate v3.0.0+incompatible
	github.com/gorilla/mux v1.7.4
)
