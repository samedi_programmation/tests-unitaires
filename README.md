############################
# Les Samedi Programmation #
############################


Samedi 25/07/2020 : Tests unitaires
===================================


Ce code source est la base du live Tests unitaires.


Il montre un exemple de tests unitaires.

- Tests unitaires sur une méthode simple
- TDD (Test Driven Development)
- Tests plus complets (proches de l'intégration) sur un service Web

Pour compiler et obtenir un exécutable, go build main.go
Pour lancer les tests, go test

Si vous avez plus de questions, vous pouvez me contacter sur https://twitch.tv/adaralex

Bonne journée à vous,

Adaralex