package main

import (
	"fmt"
	"github.com/Knetic/govaluate"
	"github.com/gorilla/mux"
	"log"
	"math"
	"net/http"
	"time"

)

func Calcul(calcul string) float64 {

/*	parametres := strings.Split(calcul, " ")

	reponse := math.NaN()

	if len(parametres) == 3 && parametres[1] == "+" {
		param1, err := strconv.ParseFloat(parametres[0], 64)
		if err != nil {
			return math.NaN()
		}
		param2, err := strconv.ParseFloat(parametres[2], 64)
		if err != nil {
			return math.NaN()
		}

		// Ici, on sait que nos deux paramètres sont des flottants
		return param1 + param2
	}

	if len(parametres) == 3 && parametres[1] == "-" {
		param1, err := strconv.ParseFloat(parametres[0], 64)
		if err != nil {
			return math.NaN()
		}
		param2, err := strconv.ParseFloat(parametres[2], 64)
		if err != nil {
			return math.NaN()
		}

		// Ici, on sait que nos deux paramètres sont des flottants
		return param1 - param2
	}

	if len(parametres) == 3 && parametres[1] == "*" {
		param1, err := strconv.ParseFloat(parametres[0], 64)
		if err != nil {
			return math.NaN()
		}
		param2, err := strconv.ParseFloat(parametres[2], 64)
		if err != nil {
			return math.NaN()
		}

		// Ici, on sait que nos deux paramètres sont des flottants
		return param1 * param2
	}

	if len(parametres) == 3 && parametres[1] == "/" {
		param1, err := strconv.ParseFloat(parametres[0], 64)
		if err != nil {
			return math.NaN()
		}
		param2, err := strconv.ParseFloat(parametres[2], 64)
		if err != nil {
			return math.NaN()
		}

		// Ici, on sait que nos deux paramètres sont des flottants
		return param1 / param2
	}

	return reponse*/

	expression, err := govaluate.NewEvaluableExpression(calcul)
	if err != nil {
		return math.NaN()
	}

	values := make(map[string]interface{}, 0)
	values["Pi"] = math.Pi
	values["Phi"] = math.Phi
	values["E"] = math.E

	resultat, err := expression.Evaluate(values)
	if err != nil {
		return math.NaN()
	}
	return resultat.(float64)
}


func ServiceCalcul(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf("%f\n", Calcul(vars["calcul"]))))
}


func main() {

	fmt.Println(Calcul("2 + 3"))

	r := mux.NewRouter()
	r.HandleFunc("/calcul/{calcul}", ServiceCalcul)

	srv := &http.Server{
		Handler:      r,
		Addr:         "127.0.0.1:8000",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}
