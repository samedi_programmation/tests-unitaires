package main

import (
	"github.com/gorilla/mux"
	"math"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestCalcul(t *testing.T) {

	if valeur := Calcul("2 + 3 - 5 * 2") ; valeur != -5 {
		t.Errorf("Le calcul n'est pas bon, 2 + 3 - 5 * 2 est censé valoir -5 et non pas %f\n", valeur)
	}


	if valeur := Calcul("2 + 3") ; valeur != 5 {
		t.Errorf("Le calcul n'est pas bon, 2 + 3 est censé valoir 5 et non pas %f\n", valeur)
	}

	if valeur := Calcul("1 + 2") ; valeur != 3 {
		t.Errorf("Le calcul n'est pas bon, 1 + 2 est censé valoir 3 et non pas %f\n", valeur)
	}

	if valeur := Calcul("1 + -2") ; valeur != -1 {
		t.Errorf("Le calcul n'est pas bon, 1 + -2 est censé valoir -1 et non pas %f\n", valeur)
	}

	if valeur := Calcul("toto + 3") ; !math.IsNaN(valeur) {
		t.Errorf("Le calcul n'est pas bon, toto + 3 est censé renvoyer NaN et non pas %f\n", valeur)
	}

	if valeur := Calcul("3 + toto") ; !math.IsNaN(valeur) {
		t.Errorf("Le calcul n'est pas bon, 3 + toto est censé renvoyer NaN et non pas %f\n", valeur)
	}

	if valeur := Calcul("hello + world") ; !math.IsNaN(valeur) {
		t.Errorf("En cas d'entrée invalide, la méthode est censée renvoyer NaN au lieu de %f\n", valeur)
	}

	if valeur := Calcul("hello world") ; !math.IsNaN(valeur) {
		t.Errorf("En cas d'entrée invalide, la méthode est censée renvoyer NaN au lieu de %f\n", valeur)
	}

	if valeur := Calcul("hello old world") ; !math.IsNaN(valeur) {
		t.Errorf("En cas d'entrée invalide, la méthode est censée renvoyer NaN au lieu de %f\n", valeur)
	}

	if valeur := Calcul("2 - 3") ; valeur != -1 {
		t.Errorf("Le calcul n'est pas bon, 2 - 3 est censé valoir -1 et non pas %f\n", valeur)
	}

	if valeur := Calcul("toto - 3") ; !math.IsNaN(valeur) {
		t.Errorf("Le calcul n'est pas bon, toto - 3 est censé renvoyer NaN et non pas %f\n", valeur)
	}

	if valeur := Calcul("3 - toto") ; !math.IsNaN(valeur) {
		t.Errorf("Le calcul n'est pas bon, 3 - toto est censé renvoyer NaN et non pas %f\n", valeur)
	}

	if valeur := Calcul("hello - world") ; !math.IsNaN(valeur) {
		t.Errorf("En cas d'entrée invalide, la méthode est censée renvoyer NaN au lieu de %f\n", valeur)
	}

	if valeur := Calcul("2 * 3") ; valeur != 6 {
		t.Errorf("Le calcul n'est pas bon, 2 * 3 est censé valoir 6 et non pas %f\n", valeur)
	}

	if valeur := Calcul("2 * 0") ; valeur != 0 {
		t.Errorf("Le calcul n'est pas bon, 2 * 0 est censé valoir 0 et non pas %f\n", valeur)
	}

	if valeur := Calcul("3 / 2") ; valeur != 1.5 {
		t.Errorf("Le calcul n'est pas bon, 3 / 2 est censé valoir 1.5 et non pas %f\n", valeur)
	}

	if valeur := Calcul("3 / 0") ; !math.IsInf(valeur, 1) {
		t.Errorf("Le calcul n'est pas bon, 3 / 0 est censé valoir +Inf et non pas %f\n", valeur)
	}

	if valeur := Calcul("Pi / Pi") ; valeur != 1 {
		t.Errorf("Le calcul n'est pas bon, Pi / Pi est censé valoir 1 et non pas %f\n", valeur)
	}

	if valeur := Calcul("10**(-3) * 15") ; valeur != 0.015 {
		t.Errorf("Le calcul n'est pas bon, 10**(-3) * 15 est censé valoir 0.015 et non pas %f\n", valeur)
	}

}


func TestServiceCalcul(t *testing.T) {


	req, err := http.NewRequest("GET", "/calcul/2%2b5", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	router := mux.NewRouter()
	router.HandleFunc("/calcul/{calcul}", ServiceCalcul)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	router.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expected := "7.000000\n"
	received := rr.Body.String()
	if received != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			received, expected)
	}

}